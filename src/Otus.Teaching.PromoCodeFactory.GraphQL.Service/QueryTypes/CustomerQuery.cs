﻿using HotChocolate.Types;
using HotChocolate.Types.Relay;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.GraphQL.Service.Types;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.GraphQL.Service.QueryTypes
{
    public class CustomerQuery
    {
        private readonly IRepository<Customer> _customerRepository;

        public CustomerQuery(IRepository<Customer> customerRepository)
        {
            _customerRepository = customerRepository;
        }

        [UsePaging(SchemaType = typeof(CustomerType))]
        [UseFiltering]
        [UseSorting]
        public async Task<IEnumerable<Customer>> Get()
        {
            return await _customerRepository.GetAllAsync();
        }

    }
}
