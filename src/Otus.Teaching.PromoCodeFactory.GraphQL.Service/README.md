# Otus.Teaching.PromoCodeFactory

## Проект для домашних заданий и демо по курсу `C# ASP.NET Core Разработчик` от `Отус`.

### Домашнее задание №5

#### Описание домашнего задания в [Wiki](https://gitlab.com/devgrav/otus.teaching.promocodefactory/-/wikis/Homework-5)

![Скриншот применения метода get](Doc/GQL_get.png "Скриншот применения метода get")

![Скриншот применения метода create](Doc/GQL_create.png "Скриншот применения метода create")